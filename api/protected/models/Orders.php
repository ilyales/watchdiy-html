<?php

/**
 * This is the model class for table "orders".
 *
 * The followings are the available columns in table 'orders':
 * @property integer $id
 * @property string $status
 * @property string $time_created
 * @property string $time_modified
 * @property string $presetName
 * @property string $caseName
 * @property string $faceName
 * @property string $handsName
 * @property string $strapName
 * @property string $text
 * @property string $image
 * @property string $customerName
 * @property string $customerEmail
 * @property string $customerPhone
 * @property string $customerAddress
 * @property string $repostVk
 * @property string $repostFb
 * @property string $repostTwitter
 */
class Orders extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'orders';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('status, time_created, time_modified, presetName', 'required'),
			array('status, presetName, caseName, faceName, handsName, strapName, customerName, customerEmail, customerPhone, customerAddress', 'length', 'max'=>255),
			array('text, image', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, status, time_created, time_modified, presetName, caseName, faceName, handsName, strapName, text, image, customerName, customerEmail, customerPhone, customerAddress, repostVk, repostFb, repostTwitter', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'status' => 'Status',
			'time_created' => 'Time Created',
			'time_modified' => 'Time Modified',
			'presetName' => 'presetName',
			'caseName' => 'Case Name',
			'faceName' => 'Face Name',
			'handsName' => 'Hands Name',
			'strapName' => 'Strap Name',
			'text' => 'Text',
			'image' => 'Image',
			'customerName' => 'Customer Name',
			'customerEmail' => 'Customer Email',
			'customerPhone' => 'Customer Phone',
			'customerAddress' => 'Customer Address',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('time_created',$this->time_created,true);
		$criteria->compare('time_modified',$this->time_modified,true);
		$criteria->compare('presetName',$this->presetName,true);
		$criteria->compare('caseName',$this->caseName,true);
		$criteria->compare('faceName',$this->faceName,true);
		$criteria->compare('handsName',$this->handsName,true);
		$criteria->compare('strapName',$this->strapName,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('customerName',$this->customerName,true);
		$criteria->compare('customerEmail',$this->customerEmail,true);
		$criteria->compare('customerPhone',$this->customerPhone,true);
		$criteria->compare('customerAddress',$this->customerAddress,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Orders the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
