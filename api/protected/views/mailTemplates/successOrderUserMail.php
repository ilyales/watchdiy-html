<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
</head>
<body style="font-family: sans-serif">
	<h2>Спасибо за ваш заказ!</h2>	
	<h3>Вы оформили заявку на производство индивидуальных именных часов на сайте www.watchn1.ru</h3>
	<p>Наш дизайнер уже работает над макетом вашего циферблата и пришлет его на согласование, как только всё будет готово!</p>
	<h3 >Детали заказа:</h3>
	<p><span style="font-weight: 700">Номер заказа: </span><?php echo $orderNum; ?></p>
	<p><span style="font-weight: 700">Корпус: </span><?php echo $caseName; ?>,</p>
	<p><span style="font-weight: 700">Циферблат: </span><?php echo $faceName; ?>,</p>
	<p><span style="font-weight: 700">Стрелки: </span><?php echo $handsName; ?>,</p>
	<p><span style="font-weight: 700">Ремешок/браслет: </span><?php echo $strapName; ?>,</p>
	<p><span style="font-weight: 700">Механизм: </span>Япония, кварц Miyota</p>
	<p>
		<span style="font-weight: 700">Изображение: </span>
		<?php if ($userImgUrl!=null): ?>
			да
		<?php else: ?>
			нет
		<?php endif;?>
	</p>
	<p>
		<span style="font-weight: 700">Текст на циферблате:  </span>
		<?php if ($userText!=null): ?>
			<?php echo $userText?>
		<?php else: ?>
			нет
		<?php endif; ?>
	</p>
	<?php if ($userText!=null): ?>
		<p><span style="font-weight: 700">Шрифт: </span><?php echo $userTextStyle?></p>
	<?php endif; ?>
	
</body>
</html>