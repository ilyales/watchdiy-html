<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
</head>
<body style="font-family: sans-serif">
	<h2>Получен заказ</h2>	
	<p><span style="font-weight: 700">Имя: </span><?php echo $name; ?></p>
	<p><span style="font-weight: 700">Телефон: </span><?php echo $phone; ?></p>
	<p>
		<span style="font-weight: 700">E-mail: </span>
		<?php if ($email!=null): ?>
			<?php echo $email; ?>
		<?php else: ?>
			не указан
		<?php endif; ?>
	</p>
	<p>
		<span style="font-weight: 700">Адрес: </span>
		<?php if ($address!=null): ?>
			<?php echo $address; ?>
		<?php else: ?>
			не указан
		<?php endif; ?>
	</p>
	<p>
		<span style="font-weight: 700">Комментарий: </span>
		<?php if ($comment!=null): ?>
			<?php echo $comment; ?>
		<?php else: ?>
			не указан
		<?php endif; ?>
	</p>

	<h2 style="margin-top:40px">Детали заказа:</h2>
	<p><span style="font-weight: 700">Номер заказа: </span><?php echo $orderNum; ?></p>
	<p><span style="font-weight: 700">Корпус: </span><?php echo $caseName; ?>,</p>
	<p><span style="font-weight: 700">Циферблат: </span><?php echo $faceName; ?>,</p>
	<p><span style="font-weight: 700">Стрелки: </span><?php echo $handsName; ?>,</p>
	<p><span style="font-weight: 700">Ремешок/браслет: </span><?php echo $strapName; ?>,</p>
	<p><span style="font-weight: 700">Механизм: </span>Япония, кварц Miyota</p>
	<p>
		<span style="font-weight: 700">Изображение: </span>
		<?php if ($userImgUrl!=null): ?>
			<a href="<?php echo $userImgUrl ?>"><?php echo $userImgUrl ?></a>
		<?php else: ?>
			нет
		<?php endif;?>
	</p>
	<p>
		<span style="font-weight: 700">Текст на циферблате:  </span>
		<?php if ($userText!=null): ?>
			<?php echo $userText?>
		<?php else: ?>
			нет
		<?php endif; ?>
	</p>
	<?php if ($userText!=null): ?>
		<p><span style="font-weight: 700">Шрифт: </span><?php echo $userTextStyle?></p>
	<?php endif; ?>
	<p><span style="font-weight: 700">Цена: </span><?php echo $price; ?> р.</p>
	
</body>
</html>