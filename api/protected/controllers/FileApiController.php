<?php

class FileApiController extends CController
{
	public function actionUploadUserImageAjax()
	{
    if ($_FILES["file"]["name"]==null) return;

    $response = new AjaxResponse;

    $ds=DIRECTORY_SEPARATOR;
		$fileFolder=Yii::app()->basePath.$ds.'..'.$ds.'..'.$ds.'public'.$ds.'userImages'.$ds;
		$fileName = mt_rand().'_'.time().'.'.pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION);
		$filePath = $fileFolder.$fileName;

    try {
      move_uploaded_file($_FILES["file"]["tmp_name"], $filePath);
  		$fileUrl = "/public/userImages/".$fileName;
      $response->setDataItem('fileUrl',$fileUrl);
			$response->setDataItem('fileName',$fileName);
      $response->send();
    }
    catch(Exception $e) {
      $response->setError($e->getMessage());
      $response->send();
    }
	}
}
