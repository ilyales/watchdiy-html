$(document).ready(function(){
	$('.screen5__slider').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		prevArrow:'<div class="screen5-arrow-left"><img class="screen5-arrow-left__img" src="img/slick2/left-arrow.png"></div>',
		nextArrow:'<div class="screen5-arrow-right"><img class="screen5-arrow-right__img" src="img/slick2/right-arrow.png"></div>',
		autoplay:true,
		autoplaySpeed:8000,
		pauseOnHover:false,
	});

	$('.order-items-input__button').click(function(e){
		var name = encodeURIComponent($('.order-items-input__item').val());
		var url = "http://watchn1.ru/imennye-chasy/bright/" + name;
		window.location = url;
		e.preventDefault();
		return false;
	});

	$(".order-items-input__item").keypress(function(e) {
	    if(e.which == 13) {
	    	var name = encodeURIComponent($('.order-items-input__item').val());
	        var url = "http://watchn1.ru/imennye-chasy/bright/" + name;
			window.location = url;
	    }
	});

	var screen2TitleEl = $('.screen2__title');

	var starpAnimationStatus = "clear";

	$(window).scroll(function() {
		var pos = $(window).scrollTop();
		if(pos >= 570 && pos <=1520  ) {
			if (!screen2TitleEl.hasClass('screen2__title_visible')) {
				screen2TitleEl.addClass('screen2__title_visible');
			}
		}


		if(pos >= 470 && pos <=1520  ) {
			if (starpAnimationStatus=="clear") {
				changeStarpAnimationStatus(1,true);	
				starpAnimationStatus = "work";
			}			
		}
		if(pos >= 1520) {		
			if (screen2TitleEl.hasClass('screen2__title_visible')) {
				screen2TitleEl.removeClass('screen2__title_visible');					
			}	
			starpAnimationStatus = "clear";
		}			
	});


	function changeStarpAnimationStatus(num,visibility) {

		var selector = ".screen2__strap_"+num;
		var strapEl = $(selector);
		if (visibility && !$(strapEl).hasClass('screen2__strap_high')) {
			$(strapEl).addClass('screen2__strap_high');
			setTimeout(function(){
				$(strapEl).removeClass('screen2__strap_high');
			},300)
		} 

		num++;

		if (num<=3) {
			setTimeout(function(){
				changeStarpAnimationStatus(num,true);
			},450);
		}	
		else {
			starpAnimationStatus = "done";
		}	
	}

	function clearScreen7TextStatus() {
		for (var i=1;i<=4;i++) {
			var selector = ".screen7-explanation__text_location"+i;
			var strapEl = $(selector);
			strapEl.removeClass('screen2__strap_high');
		}
	}

	$(".header__title-img").flip({axis: 'x'});

	setInterval(function(){
		$(".header__title-img").flip('toggle');
		setTimeout(function(){
			$(".header__title-img").flip('toggle');
		},500);
	},3000);


});