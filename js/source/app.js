angular
    .module('WatchApp', ['templates',
                         'WatchApp.CacheConstructorImages',
                         'WatchApp.Order',
                         'WatchApp.WatchCanvasComponents', 
                         'WatchApp.Constructor',
                         'WatchApp.AskForm',
                         'WatchApp.CabinetSignin',
                         'WatchApp.DeliveryPage',
                         'WatchApp.Footer',
                         'ngAnimate',
                         'duScroll'
                        ])
    .factory('Router',Router)
    .controller('WatchAppController',WatchAppController);

//сервис хранит объекст роутера, что бы он был доступен во всех модулях
function Router() {
  var service = {
    init:init,
    updateLinksBinding:updateLinksBinding,
  }
  return service;
  

  function init (router) {
    service.router = router;
  }

  function updateLinksBinding() {
    var elements = document.getElementsByTagName('a');
    for(var i = 0, len = elements.length; i < len; i++) {
      if (elements[i].hasAttribute('app-route') && !elements[i].hasAttribute('app-route-binded')) {
        elements[i].addEventListener('click', function(e) {
          e.preventDefault();
          service.router.navigate(this.getAttribute('href'));
        });
        elements[i].setAttribute('app-route-binded','');
      }      
    }
  }
}

WatchAppController.$inject =  ['$scope',
                               '$http',                               
                               '$q',
                               '$compile',
                               '$document',
                               'Router',
                               'Order',
                               'CacheConstructorImages'
                              ];

function WatchAppController($scope, $http, $q,$compile,$document,Router,Order,CacheConstructorImages) {
  var vm = this;  
  
  init();

  function init() {
    Order.loadOrder().then(function(status){
      setTimeout(router,0);      
    });
  }

  function router() {
    window.onload = function() {
      CacheConstructorImages.cache();
    }
    var firstRouteAction = true;

    var router = new Navigo();
    router
      .on({
        'signin':function() {
          openCabinetSignin();
        },
        'help':function() {
          openAskForm();
        },
        'delivery':function() {
          openDelivery();
        },        
        'imennye-chasy/:presetName/:userText':function(params) {
          console.log('userText:',params.userText);
          openConstructorWithPreset(params.presetName,decodeURIComponent(params.userText));
        },
        'imennye-chasy/:presetName':function(params) {
          openConstructorWithPreset(params.presetName);
        },
        'imennye-chasy':function() {
          if (Order.status!="new") {
            openCurrentConstructor();
          }
          else {
            router.navigate('');
            $document.scrollTopAnimated(146, 500);
          }          
        },
        '':function() {
          if (firstRouteAction) {
            if (Order.status!="new") {
              router.navigate('/imennye-chasy');
            }
            else {
              openMainPage();
            }
          }
          else {
            openMainPage();
            Router.updateLinksBinding();
          }          
        },
        '*': function () {    
          if (firstRouteAction) {
            if (Order.status!="new") {
              router.navigate('/imennye-chasy');
            }
            else {
              openMainPage();
            }
          }
          else {
            openMainPage();
            Router.updateLinksBinding();
          }
        }
      })
      .resolve();
    
    firstRouteAction = false;  

    Router.init(router);
    Router.updateLinksBinding();
  }
  


  function openMainPage() {
    console.log('openMainPage');
    vm.currentPage = "main";     

    setTimeout(function(){
      var examplesEl = angular.element(document.getElementById('examples'));
      var examplesItems = $('.examples_items');
      if (!examplesItems.hasClass('slick-slider')) {
        examplesItems.slick({
          infinite: false,
          slidesToShow: 4,
          slidesToScroll: 1,
          prevArrow:'.examples_prev',
          nextArrow:'.examples_next',
        });

        setTimeout(function(){
          examplesEl.addClass('examples__visible');
        },200);
      }
    },200);
    $scope.$apply();
  }
  
  function openAskForm() {
    console.log('openAskForm()');
    if (vm.currentPage!="askForm") {
      vm.currentPage = "askForm";
      $scope.$apply();
    }
  }

  function openCabinetSignin() {
    console.log('openCabinetSignin()');
    if (vm.currentPage!="cabinet") {
      vm.currentPage='cabinet';
      $scope.$apply();
    }    
  }  

  function openDelivery() {
    console.log('openDelivery()');
    if (vm.currentPage!="delivery") {
      vm.currentPage='delivery';
      $scope.$apply();
    }
  }

  function openCurrentConstructor() {
    vm.currentPage = "constructor";
    $scope.$apply();
  }

  function openConstructorWithPreset(presetName,presetUserText){
    // if (Order.presetName!=presetName || Order.presetUserText!=presetUserText) {
    //   Order.clearOrder(); 
    //   Order.setPresetName(presetName,presetUserText);
    // }    
    Order.clearOrder(); 
    if (angular.isUndefined(presetUserText)) {
      presetUserText = null;
    }
    Order.setPresetName(presetName,presetUserText);
    vm.currentPage = "constructor";
    $scope.$apply();
  }  

}

