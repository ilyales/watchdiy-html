angular.module('OutsideClick',[]).directive('outsideClick',function($parse,$document,$timeout) {
	return {
		replace: false,

		restrict: 'A',
		link:function (scope, element, attrs) {



			function eventHandler(e) {
				var isClickInside = element[0].contains(e.target);
				if (!isClickInside) {
					scope.$apply(function(){
		          var fn = $parse(attrs['outsideClick']);
		          fn(scope, {$event : e});
              $document.off('click', eventHandler);
		      });
				}
			}

			scope.$watch(
				function(){
          var fn = $parse(attrs['outsideElementVisible']);
          var outsideElementVisible = fn(scope)
          return outsideElementVisible;
				},
				function(newValue,oldValue){
            if (newValue==true){
              $timeout(function(){
  							$document.on('click', eventHandler);
  						});
  					}
  					else{
  						$document.off('click', eventHandler);
  					}
				}
			);
		}
	}
});
