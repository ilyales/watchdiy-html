/**
 * Компонент слайдера пресетов в конструкторе
 *
 */

angular.module('WatchApp.PresetsSlider', 
               ['WatchApp.Store',
                'WatchApp.WatchCanvasComponents',
                'WatchApp.Order',
                'slickCarousel',
                'duScroll'
               ]
              )
.component('presetsSlider', {
    controller:presetsSlider,
    controllerAs: 'vm',
    templateUrl:'/js/modules/PresetsSlider/presetsslider.html',
  });

presetsSlider.$inject = ['$scope','$document','Store','Order','CanvasComponents','WatchText'];

function presetsSlider($scope,$document,Store,Order,CanvasComponents,WatchText) {
  var vm=this;

  init();

  function init() {
    vm.slickSettings = {
      slidesToShow: 5,
      slidesToScroll: 1,
      infinite:false,
      prevArrow:".js-presets-slider-arrow-prev",
      nextArrow:".js-presets-slider-arrow-next"
    }

    vm.loadPreset = loadPreset;
    vm.presets = Store.getAllPresets();

  }

  function loadPreset(preset) {
    var text = WatchText.getText();
    Order.setPresetName(preset.name,text);
    if (text=="") {
      text="Ваше имя";
    }
    CanvasComponents.setAllByPreset(preset.name,text);
   
    $document.scrollTopAnimated(500, 500);
  }
  

}