angular.module('WatchApp.WatchCanvasComponents')
  .service('WatchCase',WatchCase);

WatchCase.$inject = ['WatchCanvas'];

function WatchCase(WatchCanvas) {
  	this.wCase = null;

  	this.allCases = [];

	this.fabObCase = null;

	this.getCaseName = function() {
		if (this.wCase!=null){
			return this.wCase.name;
		}
		return null;
	}

	this.getTitle = function() {
		if (this.wCase!=null){
			return this.wCase.title;
		}
		return null;
	}

	this.getPrice = function() {
		if (this.wCase!=null){
			return this.wCase.price;
		}
		return null;
	}

	this.mouseHoverPrice = null; // сюда записывается цена элемента на котором
								 // находится мышь. Нужно для расчета надбавочной цены
	
	this.getMouseHoverPrice = function() {
		if (this.mouseHoverPrice!=null) {
			return this.mouseHoverPrice;
		}
		else {
			return this.wCase.price;
		}
	}

	/**
	 * Нарисовать корпус часов
	 * @param {object} wCase информация о корпусе
	 * @return {promise}
	 */
	this.setCase = function(wCase) {
		this.wCase = wCase;

		this.topOffset = 0;
		this.leftOffset = 0;

		var self = this;
		fabric.Image.fromURL(this.wCase.img, function(oImg) {
			oImg.set('top',self.topOffset);
			oImg.set('left',self.leftOffset);
			oImg.set('selectable', false);
			oImg.set('weight',-1);

			//oImg.filters.push(new fabric.Image.filters.Brightness({ brightness: 100 }));
			//oImg.applyFilters();

			WatchCanvas.canvas.add(oImg);
			if (self.fabObCase!=null) {
				WatchCanvas.canvas.remove(self.fabObCase);
			}
			self.fabObCase = oImg;

			WatchCanvas.renderAll();
		});

	}


}
