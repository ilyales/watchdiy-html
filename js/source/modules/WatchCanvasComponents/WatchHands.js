angular.module('WatchApp.WatchCanvasComponents')
	.service('WatchHands', WatchHands);

WatchHands.$inject = ['WatchCanvas', 'WatchCase'];

function WatchHands(WatchCanvas, WatchCase) {

	this.wHands = null;

	this.allHands = [];

	this.fabObHands = null;

	this.getHandsName = function() {
		if (this.wHands != null) {
			return this.wHands.name;
		}
		return null;
	}

	this.getTitle = function() {
		if (this.wHands != null) {
			return this.wHands.title;
		}
		return null;
	}

	this.getPrice = function() {
		if (this.wHands != null) {
			return this.wHands.price;
		}
		return null;
	}

	this.mouseHoverPrice = null; // сюда записывается цена элемента на котором
								 // находится мышь. Нужно для расчета надбавочной цены
	this.getMouseHoverPrice = function() {
		if (this.mouseHoverPrice!=null) {
			return this.mouseHoverPrice;
		}
		else {
			return this.wHands.price;
		}
	}

	/**
	 * Установить стрелки
	 * @param {object} wHands информация о циферблате
	 */
	this.setHands = function(wHands, loadedFromServer) {
		this.wHands = wHands;

		this.topOffset = 0;
		this.leftOffset = 0;

		var self = this;
		fabric.Image.fromURL(this.wHands.img, function(oImg) {
			oImg.set('top', self.topOffset);
			oImg.set('left', self.leftOffset);
			oImg.set('selectable', false);
			oImg.set('weight', -1);
			//WatchCanvas.canvas.insertAt(oImg,8,false);
			WatchCanvas.canvas.add(oImg);
			if (self.fabObHands != null) {
				WatchCanvas.canvas.remove(self.fabObHands);
			}
			self.fabObHands = oImg;

			WatchCanvas.renderAll();


		});
	}


}