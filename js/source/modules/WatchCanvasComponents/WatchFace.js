angular.module('WatchApp.WatchCanvasComponents')
  .service('WatchFace',WatchFace);

WatchFace.$inject = ['WatchCanvas','WatchCase','$q'];

function WatchFace(WatchCanvas,WatchCase,$q) {
	this.wFace = null;

	this.allFaces = [];

	this.fabObFaceFull = null;
	this.fabObFaceHole = null;

	this.holeHilight = false;

	this.mouseHoverPrice = null; // сюда записывается цена элемента на котором
								 // находится мышь. Нужно для расчета надбавочной цены

	this.getFaceName = function() {
		if (this.wFace!=null){
			return this.wFace.name;
		}
		return null;
	}

	this.getTitle = function() {
		if (this.wFace!=null){
			return this.wFace.title;
		}
		return null;
	}

	this.getPrice = function() {
		if (this.wFace!=null){
			return this.wFace.price;
		}
		return null;
	}

	this.getMouseHoverPrice = function() {
		if (this.mouseHoverPrice!=null) {
			return this.mouseHoverPrice;
		}
		else {
			return this.wFace.price;
		}
	}

	/**
	 * Установить циферблат часов
	 * @param {object} wFace информация о циферблате
	 */
	this.setFace = function(wFace) {
		var deferred = $q.defer();

		this.wFace = wFace;
		this.topOffset = 0;
		this.leftOffset = 0;

		var self = this;
		fabric.Image.fromURL(this.wFace.imgFull, function(oImgFull) {
			oImgFull.set('top',self.topOffset);
			oImgFull.set('left',self.leftOffset);
			oImgFull.set('selectable', false);
			oImgFull.set('weight',-5);



			WatchCanvas.canvas.add(oImgFull);
			if (self.fabObFaceFull!=null) {
				WatchCanvas.canvas.remove(self.fabObFaceFull);
			}
			self.fabObFaceFull = oImgFull;


			fabric.Image.fromURL(self.wFace.imgHole, function(oImgHole) {
				oImgHole.set('top',self.topOffset);
				oImgHole.set('left',self.leftOffset);
				oImgHole.set('selectable', false);
				oImgHole.set('weight',-2);


				WatchCanvas.canvas.add(oImgHole);
				if (self.fabObFaceHole!=null) {
					WatchCanvas.canvas.remove(self.fabObFaceHole);
				}
				self.fabObFaceHole = oImgHole;

				if (self.holeHilight) {
					self.setHoleHilight();
				}
				WatchCanvas.renderAll();

				deferred.resolve();
			});
		});
		
		return deferred.promise;
	}


	this.setHoleHilight = function() {
		this.fabObFaceHole.set('opacity',0.92);
		this.fabObFaceFull.filters.push(new fabric.Image.filters.Brightness({ brightness: -7 }));
		this.fabObFaceFull.applyFilters();
		this.holeHilight = true;
	}

	this.unsetHoleHilight = function()  {
		this.fabObFaceHole.set('opacity',1);
		this.fabObFaceFull.filters = [];
		this.fabObFaceFull.applyFilters();
		this.holeHilight = false;
	}
}
