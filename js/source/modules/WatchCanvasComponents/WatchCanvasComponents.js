angular
  .module('WatchApp.WatchCanvasComponents', ['WatchApp.WatchCanvas', 'WatchApp.Store', 'WatchApp.Order', 'ngFileUpload', 'OutsideClick', 'mp.colorPicker'])
  .factory('CanvasComponents', CanvasComponents);

CanvasComponents.$inject = [
  '$q',
  'Store',
  'Order',
  'WatchCanvas',
  'WatchCase',
  'WatchFace',
  'WatchHands',
  'WatchStrap',
  'WatchImage',
  'WatchText'
];

function CanvasComponents($q, Store, Order, WatchCanvas, WatchCase, WatchFace, WatchHands, WatchStrap, WatchImage, WatchText) {
  var totalPrice;
  var maxPrice;
  var mouseHoverAddPrice;
  var specification;
  var specificationStr;
  var lastComponentsState;

  var service = {
    init: init,
    setAllDefault: setAllDefault,
    setAllByPreset: setAllByPreset,
    load: load,
    setCase: setCase,
    setAllCases: setAllCases,
    setFace: setFace,
    setAllFaces: setAllFaces,
    setHands: setHands,
    setAllHands: setAllHands,
    setStrap: setStrap,
    setAllStraps: setAllStraps,
    setFont: setFont,
    setAllFonts: setAllFonts,
    setFontColor: setFontColor,
    setAllFonts: setAllFontColors,
    changeCase: changeCase,
    changeFace: changeFace,
    changeHands: changeHands,
    changeStrap: changeStrap,
    changeFont: changeFont,
    changeText: changeText,
    deleteText: deleteText,
    addImage: addImage,
    deleteImage: deleteImage,
    getAllComponentsState: getAllComponentsState,
    saveComponentsState: saveComponentsState,
    getTotalPrice: getTotalPrice,
    getMaxPrice: getMaxPrice,
    calcTotalPrice: calcTotalPrice,
    calcMouseHoverAddPrice: calcMouseHoverAddPrice,
    getMouseHoverAddPrice: getMouseHoverAddPrice,
    getSpecification: getSpecification,
    makeSpecification: makeSpecification,
    getInfoForForm: getInfoForForm,
    getPresetName:getPresetName,
  }

  return service;

  function init() {
    WatchCanvas.init();
  }

  function setAllDefault() {   
    var cases = Store.getCurrentWCases();
    setAllCases(cases);
    setCase(cases[0]);

    var faces = Store.getCurrentWFaces(cases[0].name);
    setAllFaces(faces);
    setFace(faces[0]);

    var hands = Store.getCurrentWHands(faces[0].name);
    setAllHands(hands);
    setHands(hands[0]);

    var straps = Store.getCurrentWStraps(cases[0].name);
    setAllStraps(straps);
    setStrap(straps[0]);

    var fonts = Store.getCurrentWFonts(faces[0].name);
    setAllFonts(fonts);
    var wFont = fonts[0];
    WatchText.setFont(wFont);

    var colors = Store.getAllColors();
    setAllFontColors(colors);
    setFontColor(colors[1]);

    if (WatchImage.wImage !== null) {
      WatchImage.delete();
    }

    if (WatchText.wText !== null) {
      WatchText.delete();
    }

    WatchText.createNewText();
    WatchText.needUpdateInput = true;

    calcTotalPrice();
    makeSpecification();
  }


  /**
   * Загрузить пресет
   * @param {string|object} название пресета или объект
   *
   */
  function setAllByPreset(preset,presetUserText) {

    if (typeof preset == "string") {
      preset = Store.getPresetByName(preset);
    }    
    
    if (preset==null) {
      setAllDefault();
      return;
    }

    var cases = Store.getCurrentWCases();
    setAllCases(cases);
    var wCase = Store.getCaseByName(preset.caseName);
    if (wCase==null){
      wCase = cases[0];
    } 
    setCase(wCase);

    var faces = Store.getCurrentWFaces(cases[0].name);
    setAllFaces(faces);
    var wFace = Store.getFaceByName(preset.faceName);
    if (wFace==null) {
      wFace = faces[0];
    }
    var facePromise = setFace(wFace);

    var hands = Store.getCurrentWHands(faces[0].name);
    setAllHands(hands);
    var wHands = Store.getHandsByName(preset.handsName);
    if (wHands==null) {
      wHands = hands[0];
    }
    setHands(wHands);

    var straps = Store.getCurrentWStraps(cases[0].name);
    setAllStraps(straps);
    var wStraps = Store.getStrapByName(preset.strapsName);
    if (wStraps==null) {
      wStraps = straps[0];
    }
    setStrap(wStraps);

    var fonts = Store.getCurrentWFonts(faces[0].name);
    setAllFonts(fonts);
    
    var colors = Store.getAllColors();
    setAllFontColors(colors);   

    if (WatchText.wText !== null) {
      WatchText.delete();
    }
    if (preset.text == null) {
      WatchText.setFont(fonts[0]);
      setFontColor(colors[1]);
      WatchText.createNewText();
    } else {
      if (presetUserText!=null) {
        presetUserText = WatchText.addLineBreaks(presetUserText,preset.text.maxStrLen);
        preset.text.text = presetUserText;
      }
      WatchText.loadState(preset.text);
    }
    WatchText.needUpdateInput = true;

    if (WatchImage.wImage !== null) {
      WatchImage.delete();
    }
    if (preset.image != null) {
      //т.к. в WatchImage используются размеры из WatchFace, 
      //то нужно дождаться загрузки циферблата
      facePromise.then(function(){
        WatchImage.loadState(preset.image);
      });       
    }

    calcTotalPrice();
    makeSpecification();
  }

  function load(watchComponents) {
    try {
      
      var cases = Store.getCurrentWCases();
      var wCase = Store.getCaseByName(watchComponents.caseName);
      if (wCase == null) {
        throw new Error('Case not found:' + watchComponents.caseName);
      }
      setAllCases(cases);
      setCase(wCase, true);

      var faces = Store.getCurrentWFaces(wCase.name);
      var wFace = Store.getFaceByName(watchComponents.faceName)
      setAllFaces(faces);
      var facePromise = setFace(wFace, true);

      var allhands = Store.getCurrentWHands(wFace.name);
      var wHands = Store.getHandsByName(watchComponents.handsName);
      if (wHands == null) {
        throw new Error('Hands not found:' + watchComponents.handsName);
      }

      setAllHands(allhands);
      setHands(wHands, true);

      var straps = Store.getCurrentWStraps(wCase.name);
      var wStrap = Store.getStrapByName(watchComponents.strapName);
      if (wStrap == null) {
        throw new Error('Strap not found:' + watchComponents.strapName);
      }
      setAllStraps(straps);
      setStrap(wStrap, true);

      if (watchComponents.image != null) {
        //т.к. в WatchImage используются размеры из WatchFace, 
        //то нужно дождаться загрузки циферблата
        facePromise.then(function(){
          WatchImage.loadState(watchComponents.image);
        });        
      }

      var colors = Store.getAllColors();
      setAllFontColors(colors);
      if (watchComponents.text == null) {
        setFontColor(colors[0]);
      }

      var fonts = Store.getCurrentWFonts(wFace.name);
      setAllFonts(fonts);

      if (watchComponents.text == null) {
        WatchText.setFont(fonts[0]);
      } else {
        WatchText.needUpdateInput = true;
        WatchText.loadState(watchComponents.text);

      }
      

      calcTotalPrice();
      makeSpecification();

    } catch (e) {
      console.log("Load ERROR: " + e.message);
      setAllDefault();
    }
  }

  function setCase(wCase) {
    WatchCase.setCase(wCase);
  }

  function setAllCases(cases) {
    WatchCase.allCases = cases;
  }

  function setFace(wFace) {
    var facePromise = WatchFace.setFace(wFace);
    return facePromise;
  }

  function setAllFaces(faces) {
    WatchFace.allFaces = faces;
  }

  function setHands(wHands) {
    WatchHands.setHands(wHands);

  }

  function setAllHands(hands) {
    WatchHands.allHands = hands;
  }

  function setStrap(wStrap) {
    WatchStrap.setStrap(wStrap);

  }

  function setAllStraps(straps) {
    WatchStrap.allStraps = straps;
  }

  function setFont(wFont) {
    WatchText.setFont(wFont);
  }

  function setAllFonts(fonts) {
    WatchText.setAllFonts(fonts);
  }

  function setFontColor(color) {
    WatchText.setColor(color);
  }

  function setAllFontColors(colors) {
    WatchText.allColors = colors;
  }

  function changeCase(wCase) {
    setCase(wCase);

    //обновим спсиок циферблатов, допустимых для данного корпуса
    var allFaces = Store.getCurrentWFaces(wCase.name);
    WatchFace.allFaces = allFaces;
    // проверим находится ли текущий циферблат в обновленном списке циферблатов
    var notFound = true;
    for (var i = 0; i < allFaces.length; i++) {
      if (allFaces[i].name == WatchFace.getFaceName()) {
        notFound = false;
        break;
      }
    }
    if (notFound) changeFace(allFaces[0]);

    //обновим список ремешков, допустимых для данного корпуса
    var allStraps = Store.getCurrentWStraps(wCase.name);
    WatchStrap.allStraps = allStraps;
    //проверить находится ли текущий ремешёк в обновлеено спсике ремешков
    for (var i = 0; i < allStraps.length; i++) {
      if (allStraps[i].name == WatchStrap.getStrapName()) {
        notFound = false;
        break;
      }
    }
    if (notFound) setStrap(allStraps[0]);

    calcTotalPrice();
    makeSpecification();
  }

  function changeFace(wFace) {
    setFace(wFace);
    var allHands = Store.getCurrentWHands(wFace.name);
    WatchHands.allHands = allHands;
    //проверим находится ли текущие выбранные стрелики в обновленном списке стрелок
    var notFound = true;
    for (var i = 0; i < allHands.length; i++) {
      if (allHands[i].name == WatchHands.getHandsName()) {
        notFound = false;
        break;
      }
    }
    if (notFound) setHands(allHands[0]);

    //обновим список шрфитов
    var allFonts = Store.getCurrentWFonts(wFace.name);
    setAllFonts(allFonts);
    //поверим, находится ли текущий выбранный шрифт в обновленном списке шрифтов
    var notFound = true;
    for (var i = 0; i < allFonts.length; i++) {
      if (allFonts[i].name == WatchText.getFontName()) {
        notFound = false;
        break;
      }
    }
    if (notFound) setFont(allFonts[i]);
    
    calcTotalPrice();
    makeSpecification();
  }

  function changeHands(wHands) {
    setHands(wHands);
    calcTotalPrice();
    makeSpecification();
  }

  function changeStrap(wStrap) {
    setStrap(wStrap);
    calcTotalPrice();
    makeSpecification();
  }

  function changeFont(wFont) {
    setFont(wFont);
  }

  /**
   * Обновить текст
   * @param {string} newText Текст
   * @param {bool} [lineBreak=false] добавить переносы строк
   * @param {bool} [updateInput=false] обновить поле ввода текста (используется в textInputPopup)
   */
  function changeText(newText,lineBreak,updateInput) {
    lineBreak = typeof lineBreak !== 'undefined' ? lineBreak : false;
    updateInput = typeof updateInput !== 'undefined' ? updateInput : false;

    if (lineBreak && Order.presetName!="default") {      
      var preset = Store.getPresetByName(Order.presetName);
      newText = WatchText.addLineBreaks(newText,preset.text.maxStrLen);
    }

    if (updateInput) {
      WatchText.needUpdateInput = true;
    }

    WatchText.changeText(newText);
    makeSpecification();
  }

  function deleteText() {
    WatchText.deleteText();
    makeSpecification();
  }

  function addImage(fileUrl, fileName) {
    var wImage = {
      fileUrl: fileUrl,
      fileName: fileName,
    }
    if (WatchText.transformMode) {
      WatchText.transformModeOff();
    }
    WatchImage.setImage(wImage).then(function () {
      WatchImage.transformModeOn();
      makeSpecification();
    });
  }

  function deleteImage() {
    WatchImage.delete();
    makeSpecification();
  }

  function getAllComponentsState() {
    var componentsState = {};

    componentsState.caseName = WatchCase.getCaseName();
    componentsState.faceName = WatchFace.getFaceName();
    componentsState.handsName = WatchHands.getHandsName();
    componentsState.strapName = WatchStrap.getStrapName();
    componentsState.image = WatchImage.getImageState();
    componentsState.text = WatchText.getTextState();

    return componentsState;
  }

  function isComponentsStateChanged(newComponentsState, lastComponentsState) {
    if (lastComponentsState == null) return true;
    if (newComponentsState.caseName != lastComponentsState.caseName) return true;
    if (newComponentsState.faceName != lastComponentsState.faceName) return true;
    if (newComponentsState.handsName != lastComponentsState.handsName) return true;
    if (newComponentsState.strapName != lastComponentsState.strapName) return true;
    if (newComponentsState.image != lastComponentsState.image) return true;
    if (newComponentsState.text != lastComponentsState.text) return true;
    return false;
  }


  /**
   * Сохрнаить состояние конструктора, если произошли изменения относительно прошлого состояния
   * @param bool saveForce - принудительно созранить вне зависимости от прошлого состояния
   */

  function saveComponentsState(saveForce) {
    if (saveForce == null) {
      saveForce = false;
    }
    var newComponentsState = getAllComponentsState();
    if (saveForce) {
      Order.saveAllComponentsState(newComponentsState);
      lastComponentsState = newComponentsState;
    } else {
      var newComponentsState = getAllComponentsState();
      var compareResutl = isComponentsStateChanged(newComponentsState, lastComponentsState);
      if (compareResutl) {
        Order.saveAllComponentsState(newComponentsState);
        lastComponentsState = newComponentsState;
      }
    }
  }

  function thousandsSeparators(x) {
    if (x!=null) {
      return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    }
    else {
      return null;
    }
  }

  function getTotalPrice() {
    return thousandsSeparators(totalPrice);
  }

  function getMaxPrice() {
    return thousandsSeparators(maxPrice);
  }

  function calcTotalPrice() {
    totalPrice = WatchCase.getPrice() + WatchFace.getPrice() + WatchHands.getPrice() + WatchStrap.getPrice();
    var _maxPrice = WatchCase.getPrice();
    var maxItemPrice = 0;

    //максиальная цена из текущих циферблатов
    for (var i = 0; i < WatchFace.allFaces.length; i++) {
      if (WatchFace.allFaces[i].price > maxItemPrice) {
        maxItemPrice = WatchFace.allFaces[i].price;
      }
    }
    _maxPrice += maxItemPrice;

    //максиальная цена из всех стрелок, которые могут быть для выбранного корпуса
    maxItemPrice = 0;
    var handsForCase = Store.getWHandsForCase(WatchCase.getCaseName());
    for (var i = 0; i < handsForCase.length; i++) {
      if (handsForCase[i].price > maxItemPrice) {
        maxItemPrice = handsForCase[i].price;
      }
    }
    _maxPrice += maxItemPrice;

    //максиальная цена из текущих ремешков
    maxItemPrice = 0;
    for (var i = 0; i < WatchStrap.allStraps.length; i++) {
      if (WatchStrap.allStraps[i].price > maxItemPrice) {
        maxItemPrice = WatchStrap.allStraps[i].price;
      }
    }
    _maxPrice += maxItemPrice;

    maxPrice = _maxPrice;
  }

  function calcMouseHoverAddPrice() {
    var newTotalPrice = WatchCase.getPrice() + WatchFace.getMouseHoverPrice() + WatchHands.getMouseHoverPrice() + WatchStrap.getMouseHoverPrice();
    mouseHoverAddPrice = newTotalPrice - totalPrice;
  }

  function getMouseHoverAddPrice() {
    return mouseHoverAddPrice;
  }

  function getSpecification() {
    return specification;
  }

  function getSpecificationStr() {
    return specificationStr;
  }

  function makeSpecification() {
    specificationStr = "Корпус: " + WatchCase.getTitle() + ", Циферблат: " + WatchFace.getTitle() +
      ", Стрелки: " + WatchHands.getTitle() + ", " + WatchStrap.getTitle() +
      ", Механизм: Япония, кварц Miyota";

    specification = {
      'case': WatchCase.getTitle(),
      'face': WatchFace.getTitle(),
      'hands': WatchHands.getTitle(),
      'strap': WatchStrap.getTitle(),
    }

    if (WatchImage.getImageState() !== null) {
      specification.image = true;
    } else {
      specification.image = false;
    }

    var text = WatchText.getText();

    if (text!== null && text!=="") {
      specification.text = true;
    } else {
      specification.text = false;
    }
  }

  function getInfoForForm() {
    var info = {
      'watchImg': WatchCanvas.getCanvasImage(),
      'case': WatchCase.getTitle(),
      'face': WatchFace.getTitle(),
      'hands': WatchHands.getTitle(),
      'strap': WatchStrap.getTitle(),
      'userImg': WatchImage.getFileName(),
      'userText': WatchText.getText(),
      'userTextStyle': WatchText.getTextStyle(),
      'price':totalPrice,
    }
    return info;
  }

  function getPresetName() {
    return Order.presetName;
  }
}