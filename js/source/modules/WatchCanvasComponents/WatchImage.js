angular.module('WatchApp.WatchCanvasComponents')
	.service('WatchImage', WatchImage);

WatchImage.$inject = ['$q', 'Upload', 'WatchCanvas', 'WatchCase', 'WatchFace'];

function WatchImage($q, Upload, WatchCanvas, WatchCase, WatchFace) {
	//ширина и высота прямоуголника по центру холста, за пределы которого картинку нельзя переместить
	var allowableRectWidth = 300;
	var allowableRectHeight = 300;

	var allowableRect;

	function createAllwableRect() {
		allowableRect = new fabric.Rect({
			left: (WatchCanvas.canvas.width - allowableRectWidth)/2,
			top: (WatchCanvas.canvas.height - allowableRectHeight)/2,		
			fill: 'rgba(0,0,0)',
			width: allowableRectWidth,
			height: allowableRectHeight,
		});
	}

	

	this.wImage = null;
	this.fabObImage = null;
	this.fabObBorder = null;
	this.transformMode = false;
	this.imageState = null; // состояние картинки, для отпраки на сервер



	this.getFileUrl = function() {
		if (this.wImage != null) {
			return this.wImage.fileUrl;
		}
		return null;
	}

	this.getFileName = function() {
		if (this.wImage != null) {
			return this.wImage.fileName;
		}
		return null;
	}

	this.saveImageState = function() {
		if (this.wImage != null) {
			var self = this;
			self.imageState = {
				fileName: self.wImage.fileName,
				fileUrl: self.wImage.fileUrl,
				top: self.fabObImage.top,
				left: self.fabObImage.left,
				scaleX: self.fabObImage.scaleX,
				scaleY: self.fabObImage.scaleY,
			};
		} else {
			this.imageState = null;
		}
	}

	this.getImageState = function() {
		return this.imageState;
	}

	this.loadState = function(imageState) {
		var wImage = {
			fileUrl: imageState.fileUrl,
			top: imageState.top,
			left: imageState.left,
			scaleX: imageState.scaleX,
			scaleY: imageState.scaleY,
		}
		this.setImage(wImage);
	}

	this.uploadImage = function(file) {
		this.imageState = "loadigng";
		var promise = $q(function(resolve, reject) {
			Upload.upload({
				url: '/api/fileApi/UploadUserImageAjax',
				data: {
					file: file
				}
			}).then(function(resp) {
				if (resp.data.status) {
					var fileInfo = {
						fileUrl:resp.data.fileUrl,
						fileName:resp.data.fileName,
					}
					resolve(fileInfo);

				}
			}, function(resp) {
				//console.log('Error status: ' + resp.status);
				reject();
			}, function(evt) {});
		});
		return promise;
	}

	/**
	 * Установить картинку
	 * @param {object} wImage информация о картинке
	 */
	this.setImage = function(wImage) {
		var deferred = $q.defer();

		if (allowableRect==null) {
			createAllwableRect();
		}

		this.wImage = wImage;

		this.topOffset = WatchCase.topOffset;
		this.leftOffset = WatchCase.leftOffset;
		var self = this;


		fabric.Image.fromURL(this.wImage.fileUrl, function(oImg) {
			oImg.set('selectable', false);
			oImg.set('weight', -4);
			oImg.set('opacity', 1);
			oImg.set('originX', 'center');
			oImg.set('originY', 'center');


			if (oImg.width > WatchFace.fabObFaceFull.width / 3 || oImg.height > WatchFace.fabObFaceFull.height / 3) {
				if (oImg.width > oImg.height) {
					oImg.set('height', ((WatchFace.fabObFaceFull.width / 3) / oImg.width) * oImg.height);
					oImg.set('width', WatchFace.fabObFaceFull.width / 3);
				} else {
					oImg.set('width', ((WatchFace.fabObFaceFull.height / 3) / oImg.height) * oImg.width);
					oImg.set('height', WatchFace.fabObFaceFull.height / 3);
				}
			}

			if (self.wImage.top != null) {
				oImg.set('top', self.wImage.top);
			} else {
				oImg.set('top', WatchFace.fabObFaceFull.top + WatchFace.fabObFaceFull.height / 2);
			}

			if (self.wImage.left != null) {
				oImg.set('left', self.wImage.left);
			} else {
				oImg.set('left', WatchFace.fabObFaceFull.left + WatchFace.fabObFaceFull.width / 2);
			}

			if (self.wImage.scaleX != null) {
				oImg.set('scaleX', self.wImage.scaleX);
			}
			if (self.wImage.scaleY != null) {
				oImg.set('scaleY', self.wImage.scaleY);
			}

			WatchCanvas.canvas.add(oImg);
			if (self.fabObImage != null) {
				WatchCanvas.canvas.remove(self.fabObImage);
			}
			self.fabObImage = oImg;

			if (self.fabObBorder != null) {
				WatchCanvas.canvas.remove(self.fabObBorder);
			}

			WatchCanvas.renderAll();
			self.saveImageState();
			deferred.resolve();
		});

		return deferred.promise;
	}

	this.transformModeOn = function() {
		this.transformMode = true;
		WatchFace.setHoleHilight();
		var self = this;

		self.fabObBorder = new fabric.Rect({
			left: self.fabObImage.left,
			top: self.fabObImage.top,
			fill: 'rgba(0,0,0,0)',
			stroke: '#3066f1',
			strokeWidth: 1,
			width: self.fabObImage.width,
			height: self.fabObImage.height,
			scaleX: self.fabObImage.scaleX,
			scaleY: self.fabObImage.scaleY,
			angle: self.fabObImage.angle,
			originX: 'center',
			originY: 'center',
			weight: 1,
			selectable: true,
		});

		self.fabObBorder.setControlsVisibility({
			tl: true,
			tr: true,
			bl: true,
			br: true,
			mt: false,
			mb: false,
			ml: false,
			mr: false,
		});

		WatchCanvas.canvas.add(self.fabObBorder);
		WatchCanvas.canvas.setActiveObject(self.fabObBorder);

		self.fabObBorder.on('deselected', function() {
			setTimeout(function() {
				WatchCanvas.canvas.setActiveObject(self.fabObBorder);
			}, 0);
		});

		var goodtop, goodleft, goodscaleX, goodscaleY, goodangle;

		

		self.fabObBorder.on('moving', function() {
			self.fabObBorder.setCoords();
			if (self.fabObBorder.isContainedWithinObject(allowableRect)) {
				self.fabObImage.left = self.fabObBorder.left;
				self.fabObImage.top = self.fabObBorder.top;
				goodleft = self.fabObBorder.left;
				goodtop = self.fabObBorder.top;
			} else {
				self.fabObBorder.left = goodleft;
				self.fabObBorder.top = goodtop;
			}

		});

		self.fabObBorder.on('scaling', function() {
			self.fabObBorder.setCoords();
			if (self.fabObBorder.isContainedWithinObject(allowableRect)) {
				self.fabObImage.scaleX = self.fabObBorder.scaleX;
				self.fabObImage.scaleY = self.fabObBorder.scaleY;
				self.fabObImage.left = self.fabObBorder.left;
				self.fabObImage.top = self.fabObBorder.top;
				goodleft = self.fabObBorder.left;
				goodtop = self.fabObBorder.top;
				goodscaleX = self.fabObBorder.scaleX;
				goodscaleY = self.fabObBorder.scaleY;
			} else {
				self.fabObBorder.left = goodleft;
				self.fabObBorder.top = goodtop;
				self.fabObBorder.scaleX = goodscaleX;
				self.fabObBorder.scaleY = goodscaleY;
			}

		});

		self.fabObBorder.on('rotating', function() {
			self.fabObBorder.setCoords();
			if (self.fabObBorder.isContainedWithinObject(allowableRect)) {
				self.fabObImage.angle = self.fabObBorder.angle;
				goodangle = self.fabObBorder.angle;
			} else {
				self.fabObBorder.angle = goodangle;
			}
		});

		WatchCanvas.renderAll();
	}

	this.transformModeOff = function() {
		this.transformMode = false;
		if (this.fabObBorder != null) {
			WatchFace.unsetHoleHilight();
			WatchCanvas.canvas.remove(this.fabObBorder);
			WatchCanvas.renderAll();
			this.saveImageState();
		}
	}

	this.delete = function() {
		if (this.wImage != null) {
			if (this.transformMode) {
				this.transformModeOff();
			}
			this.fabObImage.remove();
			this.wImage = null;
			WatchCanvas.renderAll();
			this.saveImageState();
		}
	}
}