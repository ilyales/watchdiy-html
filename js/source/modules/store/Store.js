/**
 *	Хранилище компонентов часов - получает данные из файлов конфигурации
 *  Предоставляет методы получения актуального списка компонентов согласно их иерархии
 *  Например в зависмости от установленного корпуса getCurrentWFaces будет возвращать
 *  допустимы для него циферблаты
 *
 */
angular
		.module('WatchApp.Store', ['WatchApp.Config'])
		.service('Store',Store);

Store.$inject = [
		'$http',
		'CasesConf',
		'FacesConf',
		'HandsConf',
		'StrapsConf',
		'FontsConf',
		'FontColorsConf',
		'PresetsConf'
	];


function Store ($http,CasesConf,FacesConf,HandsConf,StrapsConf,FontsConf,FontColorsConf,PresetsConf) {
	//задаёт режим конфигурации. В заисимости от выбранного режима методы Store будут возвращать
	//тольео те наборы элементов, которые соответствуют данному режиму. В конфиграции для элементов
	//часов для каждого элемента указаны допустимые режимы 
	var configMode = "male";	// male|female|all

	var elementsByConfigMode = {
		wCases:null,
		wFaces:null,
		wHands:null,
		wStraps:null,
	}

	this.setConfigMode = setConfigMode;
	this.setConfigModeByPreset = setConfigModeByPreset;
	this.getCurrentWCases = getCurrentWCases;
	this.getCaseByName = getCaseByName;
	this.getCurrentWFaces = getCurrentWFaces;
	this.getFaceByName = getFaceByName;
	this.getCurrentWHands = getCurrentWHands;
	this.getWHandsForCase = getWHandsForCase;
	this.getHandsByName = getHandsByName;
	this.getCurrentWStraps = getCurrentWStraps;
	this.getStrapByName = getStrapByName;
	this.getCurrentWFonts = getCurrentWFonts;
	this.getFontByName = getFontByName;
	this.getAllColors = getAllColors;
	this.getPresetByName = getPresetByName;
	this.getAllPresets = getAllPresets;


	function setConfigMode(_configMode) {
		сonfigMode = _configMode;
		elementsByConfigMode = {
			wCases:null,
			wFaces:null,
			wHands:null,
			wStraps:null,
		}
	}

	function setConfigModeByPreset(_presetName) {
		var preset = getPresetByName;
		if (preset!=null && preset.configMode!=null) {
			setConfigMode(preset.configMode);
			return true;
		}
		else {
			return false;
		}
	}

	
	function getCurrentWCases() {
		var wCases = getWCasesByConfigMode();
		return wCases;
	}

	function getCaseByName(name) {
		var wCase = null;
		for (var i=0;i<CasesConf.items.length;i++) {
			if (CasesConf.items[i].name==name) {
				wCase = CasesConf.items[i];
				break;
			}
		}
		return wCase;
	}

	function getCurrentWFaces(selectedCaseName) {
		var wFaces = getWFacesByConfigMode();
		var current_wFaces = [];

		for (var i=0;i<wFaces.length;i++) {
			var validCases = wFaces[i]['validCases'];
			var validFace = false;
			for (var j=0;j<validCases.length;j++)
			{
				if (validCases[j]==selectedCaseName) {
					validFace = true;
					break;
				}
			}
			if (validFace) {
				current_wFaces.push(wFaces[i]);
			}
		}
		return current_wFaces;
	}

	function getFaceByName(name) {
		var wFace = null;
		for (var i=0;i<FacesConf.items.length;i++) {
			if (FacesConf.items[i].name==name) {
				wFace = FacesConf.items[i];
				break;
			}
		}
		return wFace;
	}

	function getCurrentWHands(selectedFaceName) {
		var wHands = getWHandsByConfigMode();
		var current_wHands = [];
		for (var i=0;i<wHands.length;i++) {
			var validFaces = wHands[i]['validFaces'];
			var validHands = false;
			for (var j=0;j<validFaces.length;j++)
			{
				if (validFaces[j]==selectedFaceName) {
					validHands = true;
					break;
				}
			}

			if (validHands) {
				current_wHands.push(wHands[i]);
			}
		}

		return current_wHands;
	}


	//для расчета максмальной цены комплектации
	function getWHandsForCase(selectedCaseName) {
		var wHands = getWHandsByConfigMode();
		var current_wHands = [];
		var facesForCase = this.getCurrentWFaces(selectedCaseName);
		for (var i=0;i<wHands.length;i++) {
			validFaces =  wHands[i]['validFaces'];
			validHands = false;
			for (var j=0;j<validFaces.length;j++) {
				for (var k=0;k<facesForCase.length;k++) {
					if (validFaces[j]==facesForCase[k].name) {
						validHands = true;
						break;
					}
				}
				if (validHands) break;
			}

			if (validHands) {
				current_wHands.push(wHands[i]);
			}	
		}
		return current_wHands;
	}

	function getHandsByName(name) {
		var wHands = null;
		for (var i=0;i<HandsConf.items.length;i++) {
			if (HandsConf.items[i].name==name) {
				wHands = HandsConf.items[i];
				break;
			}
		}
		return wHands;
	}


	function getCurrentWStraps(selectedCaseName) {
		var wStraps = getWStrapsByConfigMode();
		var current_wStraps = [];
		for (var i=0;i<wStraps.length;i++) {
				var validCases = wStraps[i]['validCases'];
				var validStrap = false;
				for (var j=0;j<validCases.length;j++)
				{
					if (validCases[j]==selectedCaseName) {
						validStrap = true;
						break;
					}
				}
				if (validStrap) {
					current_wStraps.push(wStraps[i]);
				}
		}
		return current_wStraps;
	}

	function getStrapByName(name) {
		var wStrap = null;
		for (var i=0;i<StrapsConf.items.length;i++) {
			if (StrapsConf.items[i].name==name) {
				wStrap = StrapsConf.items[i];
				break;
			}
		}
		return wStrap;
	}

	function getCurrentWFonts(selectedFaceName) {
		var current_wFonts = [];
		for (var i=0;i<FontsConf.items.length;i++) {
			var validFaces = FontsConf.items[i]['validFaces'];
			var validFont = false;
			for (var j=0;j<validFaces.length;j++)
			{
				if (validFaces[j]==selectedFaceName) {
					validFont = true;
					break;
				}
			}

			if (validFont) {
				current_wFonts.push(FontsConf.items[i]);
			}
		}
		return current_wFonts;
	}

	function getFontByName(name) {
		var wFont = null;
		for (var i=0;i<FontsConf.items.length;i++) {
			if (FontsConf.items[i].name==name) {
				wFont = FontsConf.items[i];
				break;
			}
		}
		return wFont;
	}

	function getAllColors() {
		return FontColorsConf.items;
	}




	function getWCasesByConfigMode() {
		if (elementsByConfigMode.wCases==null){
			elementsByConfigMode.wCases = [];
			for (var i=0;i<CasesConf.items.length;i++) {
				var hasCurrentConfigMode = false;
				for (var j=0;j<CasesConf.items[i].configModes.length;j++){
					if (CasesConf.items[i].configModes[j]==configMode){
						hasCurrentConfigMode = true;
						break;
					}
				}
				if (hasCurrentConfigMode) {
					elementsByConfigMode.wCases.push(CasesConf.items[i]);
				}			
			}
		}		
		return elementsByConfigMode.wCases;
	}

	function getWFacesByConfigMode() {
		if (elementsByConfigMode.wFaces==null){
			elementsByConfigMode.wFaces = [];
			for (var i=0;i<FacesConf.items.length;i++) {
				var hasCurrentConfigMode = false;
				for (var j=0;j<FacesConf.items[i].configModes.length;j++){
					if (FacesConf.items[i].configModes[j]==configMode){
						hasCurrentConfigMode = true;
						break;
					}
				}
				if (hasCurrentConfigMode) {
					elementsByConfigMode.wFaces.push(FacesConf.items[i]);
				}			
			}
		}		
		return elementsByConfigMode.wFaces;
	}


	function getWHandsByConfigMode() {
		if (elementsByConfigMode.wHands==null){
			elementsByConfigMode.wHands = [];
			for (var i=0;i<HandsConf.items.length;i++) {
				var hasCurrentConfigMode = false;
				for (var j=0;j<HandsConf.items[i].configModes.length;j++){
					if (HandsConf.items[i].configModes[j]==configMode){
						hasCurrentConfigMode = true;
						break;
					}
				}
				if (hasCurrentConfigMode) {
					elementsByConfigMode.wHands.push(HandsConf.items[i]);
				}			
			}
		}		
		return elementsByConfigMode.wHands;
	}

	function getWStrapsByConfigMode() {
		if (elementsByConfigMode.wStraps==null){
			elementsByConfigMode.wStraps = [];
			for (var i=0;i<StrapsConf.items.length;i++) {
				var hasCurrentConfigMode = false;
				for (var j=0;j<StrapsConf.items[i].configModes.length;j++){
					if (StrapsConf.items[i].configModes[j]==configMode){
						hasCurrentConfigMode = true;
						break;
					}
				}
				if (hasCurrentConfigMode) {
					elementsByConfigMode.wStraps.push(StrapsConf.items[i]);
				}			
			}
		}		
		return elementsByConfigMode.wStraps;
	}

	function getPresetByName(_name) {
		var preset=null;
		for (var i=0;i<PresetsConf.items.length;i++) {
			if (PresetsConf.items[i].name===_name) {
				preset = PresetsConf.items[i];
			}
		}
		return preset;
	}

	function getAllPresets() {
		return PresetsConf.items;
	}

}
