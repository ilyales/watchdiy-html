angular.module('WatchApp.WatchControlComponents')
  .component('tooltips', {
    controller:tooltips,
    controllerAs: 'vm',
    templateUrl:'/js/modules/WatchControlComponents/templates/tooltips.html',
  });

tooltips.$inject = ['$scope','$element','$timeout','CanvasComponents','WatchText','ConstructorTips'];

function tooltips($scope,$element,$timeout,CanvasComponents,WatchText,ConstructorTips) {
  var vm=this,
      tips=['fonts'],
      textControlPanelEl;
  vm.currentTip = null;
  vm.close = close;
  init();

  function init() {
    initScopeWatchs();    
  }

  function initScopeWatchs() {
    $scope.$watch(
      function(){
        return ConstructorTips.getCurrentTip();
      },
      function(newValue,oldValue){
        if (ConstructorTips.isActive && isToolTip(newValue) && vm.currentTip!=newValue){
          show(newValue);  
        }      
        if (ConstructorTips.isActive && vm.currentTip!=null && !isToolTip(newValue)) {
          vm.currentTip = null;
        }
      }
    );  
  }

  function isToolTip(_tipName){
    var result = false;
    for (var i=0;i<tips.length;i++) {
      if (tips[i]==_tipName) {
        result = true;
        break;
      }
    }
    return result;
  }

  function show(_tipName) {
    $timeout(function(){
      vm.currentTip = _tipName;
      if (_tipName=="fonts") {
        textControlPanelEl = angular.element(document.querySelector('.text-control_control-panel'));
        textControlPanelEl.on("click",closeFonts);
      }
    },1000);

    
  }

  function close() {
    ConstructorTips.nextTip();    
  }

  function closeFonts() {
    textControlPanelEl.off("click",closeFonts);
    close();
    $scope.$digest();
  }
}