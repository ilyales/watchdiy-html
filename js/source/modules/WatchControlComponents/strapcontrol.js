angular.module('WatchApp.WatchControlComponents')
  .component('strapcontrol', {
    controller:strapcontrol,
    controllerAs: 'vm',
    replace:true,
    templateUrl:'/js/modules/WatchControlComponents/templates/strapcontrol.html',
  });

strapcontrol.$inject = ['$scope','$element','CanvasComponents','WatchStrap'];

function strapcontrol($scope,$element,CanvasComponents,WatchStrap) {
  var vm = this;

  vm.allStraps = null;
  vm.selectedStrapName = null;
  vm.selectedStrapType = "leather";
  vm.currentTypeStraps = null;

  

  vm.changeType = function(type) {
    vm.selectedStrapType = type;
    upadteCurrentTypeStraps();
  }

  vm.changeStrap= function(wStrap) {
    CanvasComponents.changeStrap(wStrap);
    vm.selectedStrapName = wStrap.name;
    WatchStrap.mouseHoverPrice = null;
    CanvasComponents.calcMouseHoverAddPrice();
  }

  vm.mouseOnItem = function(item) {
    if (item!=null) {
      WatchStrap.mouseHoverPrice = item.price;
    }
    else {
      WatchStrap.mouseHoverPrice = null;
    }
    CanvasComponents.calcMouseHoverAddPrice();
  }


  $scope.$watch(
    function(){
      return WatchStrap.getStrapName();
    },
    function(newValue,oldValue){
      if (newValue!=oldValue || vm.selectedStrapName==null){
        vm.selectedStrapName = newValue;
      }
    }
  );

  var controlItemsInnerEl = angular.element($element[0].querySelector('.constructor-control_items-inner'));
  var controlItemsNode = $element[0].querySelector('.constructor-control_items');

  $scope.$watch(
    function(){
      return WatchStrap.allStraps;
    },
    function(newValue,oldValue){

      if (newValue!=oldValue || vm.allHands==null ){
        vm.allStraps = WatchStrap.allStraps;           
        upadteCurrentTypeStraps();
      }

    },
    true
  );

  function upadteCurrentTypeStraps() {
    vm.currentTypeStraps = [];
    for (var i=0;i<WatchStrap.allStraps.length;i++) {
      if (WatchStrap.allStraps[i].strapType==vm.selectedStrapType) {
        vm.currentTypeStraps.push(WatchStrap.allStraps[i]);
      }
    } 
  }
}
