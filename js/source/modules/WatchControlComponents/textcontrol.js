angular.module('WatchApp.WatchControlComponents')
  .component('textcontrol', {
    controller:textcontrol,
    controllerAs: 'vm',
    templateUrl:'/js/modules/WatchControlComponents/templates/textcontrol1.html',
  });

textcontrol.$inject = ['$scope','$element','CanvasComponents','WatchText','WatchImage','injectCSS'];

function textcontrol($scope,$element,CanvasComponents,WatchText,WatchImage,injectCSS) {
  var vm=this;

  //выбранный шрифт, содержит объект из fontsConfig
  vm.selectedFont = null;
  //выбранный размер - число
  vm.selectedSize = null;
  vm.allFonts = [];
  
  var userTextInputEl = angular.element($element[0].querySelector('.text-control_textarea'));
  
  init();

  function init() {
    vm.allFonts = WatchText.allFonts;
    vm.transformModeOnBtnVisible = false;
    
    userTextInputEl.on('keyup paste',onUserTextChanged);
    userTextInputEl[0].focus();
  }

  function onUserTextChanged() {
    var newText = userTextInputEl[0].value;
    if (WatchText.getText!=newText) {
      CanvasComponents.changeText(newText);
    }  

    if (newText=="" && vm.transformModeOnBtnVisible==true) {
      vm.transformModeOnBtnVisible = false;
      $scope.$digest();
    }  
    if (newText!="" && vm.transformModeOnBtnVisible==false) {
      vm.transformModeOnBtnVisible = true;
      $scope.$digest();
    }
  }

  // $scope.$watch(
  //   function(){
  //     return WatchText.getText();
  //   },
  //   function(newValue,oldValue){
  //     //в условии ниже есть дополнительная проврка на совпадение текста с плейсхолдером.
  //     //Это обход бага IE10/11, котрый изначально в textarea.value  подставляет значение из плейсхолдера
  //     if (newValue!=oldValue || (newValue!==null && (userTextInputEl[0].value==="" || userTextInputEl[0].value===userTextInputEl[0].getAttribute('placeholder')))){
  //       userTextInputEl.val(WatchText.getText());
  //     }      
  //   }
  // );
  
  $scope.$watch(
    function(){
      return WatchText.needUpdateInput;
    },
    function(newValue,oldValue) {
      if (newValue==true) {
        userTextInputEl.val(WatchText.getText());
        WatchText.needUpdateInput = false;
      }      
    }
  );  
  

  vm.transformModeOn = function() {
    if (vm.transformModeOnBtnVisible) {
      if (WatchImage.transformMode) {
        WatchImage.transformModeOff();
      }
      WatchText.transformModeOn();
    }
  }

  $scope.$watch(
    function(){
      return WatchText.transformMode;
    },
    function(newValue, oldValue){
      if (newValue==false) {
        vm.transformModeOnBtnVisible = true;
      }
      if (newValue==true) {
        vm.transformModeOnBtnVisible = false;
      }
    }
  );

  vm.clearText = function() {
    userTextInputEl.val('');
    CanvasComponents.changeText('');
  }

  vm.fontFamilyDropDown = {
    listVisible:false,
    showList:function() {
      this.listVisible = true;
    },
    closeList:function() {
      this.listVisible = false;
    },
    changeListVisible:function() {
      this.listVisible = !this.listVisible;
    },
    select:function(font) {
      WatchText.setFont(font);
      this.listVisible = false;
    }
  }

  $scope.$watch(
    function(){
      return WatchText.getFontName();
    },
    function(newValue,oldValue){
      if (newValue!=oldValue || vm.selectedFont==null){
        vm.selectedFont = WatchText.wFont;

      }
    }
  );

  $scope.$watch(
    function(){
      return WatchText.allFonts;
    },
    function(newValue,oldValue){
      if (newValue!=oldValue || vm.allFonts==null){
        vm.allFonts = newValue;
        console.log('vm.allFonts',vm.allFonts);
      }
    }
  );


  vm.fontSizeDropDown = {
    listVisible:false,
    showList:function() {
      this.listVisible = true;
    },
    closeList:function() {
      this.listVisible = false;
    },
    changeListVisible:function() {
      this.listVisible = !this.listVisible;
    },

    select:function(size) {
      WatchText.setFontSize(size);
      this.listVisible = false;
    }
  }

  $scope.$watch(
    function(){
      return WatchText.fontSize;
    },
    function(newValue,oldValue){
      if (newValue!=oldValue || vm.selectedSize==null){
        vm.selectedSize = newValue;
      }
    }
  );


  vm.colorPalette = {
    colorsListVisible : false,
    pickerVisible : false,

    allColors:null,

    currentColor : null,
    colorPickerColor: null,

    changeColor : function(color) {
      WatchText.setColor(color);
      this.colorsListVisible = false;
    },
    showColorsList : function() {
      this.colorsListVisible = true;
    },
    closeColorsList : function() {
      this.colorsListVisible = false;
    },
    showPicker : function() {
      this.colorsListVisible = false;
      this.pickerVisible = true;      
      console.log('showPicker');
    },
    closePicker : function() {
      this.pickerVisible = false;
    }
  }

  $scope.$watch(
    function(){
      return WatchText.allColors;
    },
    function(newValue,oldValue){

      if (newValue!=oldValue || vm.colorPalette.allColors==null){
        vm.colorPalette.allColors = newValue;

      }
    }
  );

  $scope.$watch(
    function(){
      return WatchText.color;
    },
    function(newValue,oldValue){
      if (newValue!=oldValue || vm.colorPalette.currentColor==null){
        vm.colorPalette.currentColor = newValue;
        vm.colorPalette.colorPickerColor = newValue;
      }
    }
  );

  $scope.$watch(
    function(){
      return vm.colorPalette.colorPickerColor;
    },
    function(newValue,oldValue){
      if (newValue!=oldValue){
        WatchText.setColor(newValue);
      }
    }
  );

  
}
