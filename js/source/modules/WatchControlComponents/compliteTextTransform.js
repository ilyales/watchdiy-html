angular.module('WatchApp.WatchControlComponents')
  .component('compliteTextTransform', {
    controller:compliteTextTransform,
    controllerAs: 'vm',
    templateUrl:'/js/modules/WatchControlComponents/templates/compliteTextTransform.html',
  });

compliteTextTransform.$inject = ['$scope','WatchText'];

function compliteTextTransform($scope,WatchText) {
  var vm=this;
  vm.complite = function(){
    WatchText.transformModeOff();
  }

  vm.visible = false;

  $scope.$watch(
    function(){
      return WatchText.transformMode;
    },
    function(newValue, oldValue){
      if (newValue==true) {
        vm.visible = true;
      }
      else {
        vm.visible = false;
      }
    }
  );
}
