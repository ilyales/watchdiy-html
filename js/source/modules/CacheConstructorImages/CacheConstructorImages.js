angular.module('WatchApp.CacheConstructorImages',['WatchApp.Config'])
  .service('CacheConstructorImages',CacheConstructorImages);

CacheConstructorImages.$inject = ['CasesConf',
								  'FacesConf',
								  'HandsConf',
								  'StrapsConf'];

function CacheConstructorImages(CasesConf,FacesConf,HandsConf,StrapsConf) {
	console.log('CacheConstructorImages');	

	this.cache = cache;

	function cache() {
		loadCases();
		loadFaces();
		loadHands();
		loadStraps();
	}

	function loadCases() {
		for (var i=0;i<CasesConf.items.length;i++) {
			var img = new Image();
			img.src = CasesConf.items[i].img;
			var preView = new Image();
			preView.src = CasesConf.items[i].previewImg
		} 
	}

	function loadFaces() {
		for (var i=0;i<FacesConf.items.length;i++) {
			var imgFull = new Image();
			imgFull.src = FacesConf.items[i].imgFull;
			var imgHole = new Image();
			imgHole.src = FacesConf.items[i].imgHole;
			var preView = new Image();
			preView.src = FacesConf.items[i].previewImg
		} 
	}

	function loadHands() {
		for (var i=0;i<HandsConf.items.length;i++) {
			var img = new Image();
			img.src = HandsConf.items[i].img;
			var preView = new Image();
			preView.src = HandsConf.items[i].previewImg
		} 
	}

	function loadStraps() {
		for (var i=0;i<StrapsConf.items.length;i++) {
			var img = new Image();
			img.src = StrapsConf.items[i].img;
			var preView = new Image();
			preView.src = StrapsConf.items[i].previewImg
		} 
	}
}