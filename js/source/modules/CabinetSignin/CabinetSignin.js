angular.module('WatchApp.CabinetSignin', ['WatchApp.Order'])
	.component('cabinetsignin', {
	    controller:cabinetsignin,
	    controllerAs: 'vm',
	    templateUrl:'/js/modules/CabinetSignin/cabinetsignin.html',
	    bindings: {
			 'onSigninSuccess': '=',
		  },
	  });

cabinetsignin.$inject = ['$scope','$timeout','Order'];

function cabinetsignin($scope,$timeout,Order) {
  var vm=this;
  vm.email = null;
  vm.findByEmail = findByEmail;
  vm.notFoundInfoVisible = false;
  vm.notValidEmailInfoVisible = false;


  this.$onInit = function() {
  	document.getElementById("cabinetSignin_inp").focus();
  } 

  function findByEmail() {
  	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!re.test(vm.email)) {
    	vm.notValidEmailInfoVisible = true;
  		$timeout(function(){
  			vm.notValidEmailInfoVisible = false;
  		},2000);
  		return;
    }

  	Order.findByEmail(vm.email).then(
  		function(){
  			//vm.onSigninSuccess();
        window.location = "/constructor";
  		},
  		function(){
  			vm.notFoundInfoVisible = true;
  			$timeout(function(){
  				vm.notFoundInfoVisible = false;
  			},2000);
  		}
  	);  	
  } 


}