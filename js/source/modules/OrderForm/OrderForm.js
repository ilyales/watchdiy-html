angular.module('WatchApp.OrderForm', ['WatchApp.WatchCanvas',
                                      'WatchApp.WatchCanvasComponents',
                                      'WatchApp.Order'
                                      ]
        )

.component('orderform', {
    controller:orderform,
    controllerAs: 'vm',
    templateUrl:'/js/modules/OrderForm/orderform.html',
  });

orderform.$inject = ['$scope','Order','CanvasComponents'];

function orderform($scope,Order,CanvasComponents) {
  var vm=this;

  vm.state = "form";

  vm.sendForm = function() {
    validate();
  }

  vm.values = {
    name:"",
    phone:"",
    email:"",
    address:"",
    comment:"",
  }

  vm.validResults = {
    name:true,
    phone:true,
    email:true,
  }

  //атовразмер для поля ввода адреса в форме (autosize.min.js)
  autosize(document.querySelector('.constructor-form_text'));

  function validate() {
    vm.validResults = {
      name:true,
      phone:true,
      email:true,
    }
    var valid = true;

    if (vm.values.name==null || vm.values.name=="") {
      vm.validResults.name = false;
      valid = false;
    }

    if (vm.values.phone==null || vm.values.phone=="") {
      vm.validResults.phone = false;
      valid = false;
    }

    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (vm.values.email!="" && !re.test(vm.values.email)) {
      vm.validResults.email = false;
      valid = false;
    }

    if (valid) {
      send();
    }
  }


  function send() {
    vm.state = 'loading';

    var values = CanvasComponents.getInfoForForm();
    values.name = vm.values.name;
    values.phone = vm.values.phone;
    values.email = vm.values.email;
    values.address = vm.values.address;
    values.comment = vm.values.comment;

    Order.sendOrderForm(values).then(
      //success
      function(data){        
        console.log(data);
        vm.values = {
          name:"",
          phone:"",
          email:"",
          address:"",
        }

        vm.validResults = {
          name:true,
          phone:true,
          email:true,
        }
        vm.state = 'form';
      },
      //fail
      function() {
        console.log('fail');
      }
    );
  }

}
