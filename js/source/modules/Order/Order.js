angular.module('WatchApp.Order', ['ngCookies', 'WatchApp.Store'])
  .service('Order', Order);

Order.$inject = ['$cookies', '$http', '$q', 'Store'];

function Order($cookies, $http, $q, Store) {
  var COOKIE_NAME = "watchdiy2";

  this.loadOrder = loadOrder;
  this.createOrder = createOrder;
  this.saveAllComponentsState = saveAllComponentsState;
  this.sendOrderForm = sendOrderForm;
  this.sendAskForm = sendAskForm;
  this.sendRepostNotice = sendRepostNotice;
  this.clearOrder = clearOrder;
  this.setPresetName = setPresetName;
  this.findByEmail = findByEmail;

  this.orderId = null;
  this.status = null;
  this.presetName = 'default';
  this.presetUserText = null;
  this.components = null;

  function loadOrder() {
    var deferred = $q.defer();
    if ($cookies.get(COOKIE_NAME) != null) {

      var cookies = JSON.parse($cookies.get(COOKIE_NAME));

      var orderId = cookies.orderId;

      var url = '/api/WatchConstructorApi/getOrder';
      var self = this;
      $http.post(url).then(function (rawData) {
        var data = rawData.data;
        if (data.status) {

          var componentsEmpty = false;

          if (!angular.isUndefined(data.components) && data.components !== null) {
            self.components = JSON.parse(data.components);
            if (self.components.caseName != null && self.components.faceName != null && self.components.handsName != null && self.components.strapName != null) {
              self.orderId = orderId;
              self.status = data.orderStatus;
              setPresetName.bind(self)(data.presetName);
            } else {
              componentsEmpty = true;
            }
          } else {
            componentsEmpty = true;
          }
          console.log('componentsEmpty', componentsEmpty);

          if (componentsEmpty) {
            //если по какой-то причине один из обязательных компонентов пуст, то удаляем куку и ставим статус "new"
            $cookies.remove(COOKIE_NAME,{ path: '/' });
            self.components = null;
            self.status = "new";
          }

        } else {
          //если произошла ошибка, то удаляем куку и ставим статус "new"
          $cookies.remove(COOKIE_NAME,{ path: '/' });
          self.status = "new";
        }
        deferred.resolve(self.status);
      });
    } else {
      this.status = "new";
      deferred.resolve(this.status);
    }

    return deferred.promise;
  }

  function clearOrder() {
    this.orderId = null;
    this.status = "new";
    this.components = null;
    $cookies.remove(COOKIE_NAME,{ path: '/' });
  }

  function createOrder() {
    var deferred = $q.defer();
    var url = '/api/WatchConstructorApi/newWatch';
    var self = this;
    $http.post(url, {
      presetName: this.presetName
    }).then(function (rawData) {
      var data = rawData.data;
      if (data.status) {
        self.status = "new";
        self.orderId = data.orderId;
        deferred.resolve();
      }
    });

    return deferred.promise;
  }

  

  function setPresetName (_presetName,_presetUserText) {
    this.presetName = _presetName;
    this.presetUserText = _presetUserText;
    Store.setConfigModeByPreset(_presetName);
  }

  function saveAllComponentsState(componentsState) {
    this.components = componentsState;
    var url = "/api/watchConstructorApi/SaveAllComponentsState"
    $http.post(url, {
      componentsState: componentsState,
    }).then(function (data) {
      if (data.status) {

      } else {
        console.log('Ошибка сохранения статуса');
      }
    });
  }


  function sendOrderForm(values) {
    var url = '/api/formApi/sendOrderAjax';
        self = this;

    values.info = generateData();

    var promise = $q(function (resolve, reject) {
      $http.post(url, {
        values: values,
      }).then(
        function (data) {
          if (data.data.status) {
            self.status = "checkout";
            
            yaCounter44321164.reachGoal('order');
            
            resolve(data.data);
          } else {
            reject({
              error: data.message,
            });
          }
        });
    });
    return promise;
  }

  function sendAskForm(values) {
    var url = '/api/formApi/sendAskAjax',
        self = this;

    values.info = generateData();

    var promise = $q(function (resolve, reject) {
      $http.post(url, {
        values: values,
      }).then(
        function (data) {
          if (data.data.status) {
            resolve();
          } else {
            reject({
              error: data.message,
            });
          }
        });
    });
    return promise;
  }

  function sendRepostNotice(type) {
    var url="/api/formApi/sendRepostNotice";

    var values = {
      type:type,
      info:generateData()
    }   
    $http.post(url,values);
  }

  function generateData() {
      var date = new Date(), 
          result = 4721*date.getSeconds();
      return result;
  }


  function findByEmail(email) {
    var deferred = $q.defer();
    var self = this;
    var url = "/api/watchConstructorApi/findByEmail"
    $http.post(url, {
      email: email,
    }).then(function (rawData) {
      var data = rawData.data;
      if (data.status) {
        self.components = data.components;
        self.status = data.orderStatus;
        self.presetName = data.presetName;
        var cookieValue = {orderId:data.orderId};
        $cookies.put(COOKIE_NAME,JSON.stringify(cookieValue));
        deferred.resolve();
      } else {
        console.log('not found');
        deferred.reject();
      }
    });

    return deferred.promise;
  }

}