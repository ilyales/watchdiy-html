angular.module('WatchApp.Constructor', ['WatchApp.Order',
    'WatchApp.WatchCanvas',
    'WatchApp.WatchCanvasComponents',
    'WatchApp.WatchControlComponents',
    'WatchApp.PresetsSlider',
    'WatchApp.OrderForm',
    'WatchApp.SocialShare',
    'WatchApp.ConstructorTips',
    'duScroll'
  ])
  .component('watchconstructor', {
    controller: constructor,
    controllerAs: 'vm',
    templateUrl: '/js/modules/Constructor/constructor1.html',
  });

constructor.$inject = ['$scope',
  '$timeout',
  '$document',
  'Router',
  'Order',
  'CanvasComponents',
  'WatchText',
  'WatchImage',
  'SocialShare',
  'ConstructorTips'
];

function constructor($scope,$timeout,$document,Router, Order, CanvasComponents,WatchText,WatchImage,SocialShare,ConstructorTips) {
  var vm = this;
  var constructorInfoEl = angular.element(document.getElementById('constructor-info'));
  var constructorDescrEl = angular.element(document.getElementById('constructor-descr'));
  var Share;

  vm.specification = null;
  vm.status = null;
  vm.orderNum = null;
  vm.createNewWatch = createNewWatch;
  vm.fontsReady = false;

  vm.shareVk = shareVk;
  vm.shareFacebook = shareFacebook;
  vm.shareTwitter = shareTwitter;
  vm.shippingRepostStatus = "wait";
  
  this.$onInit = init;
   

  function init() {

    CanvasComponents.init();
    Router.updateLinksBinding();

    

    if (Order.status == "new") {
      Order.createOrder().then(function() {     
        vm.status = "edit";
        Order.status = "edit";
        if (Order.presetName=="default") {
          CanvasComponents.setAllDefault();
        }
        else {
          CanvasComponents.setAllByPreset(Order.presetName,Order.presetUserText);
        }
    
        if (Order.presetUserText==null) {
          ConstructorTips.showTips();
        }
        else {
          ConstructorTips.showTips("fonts");  
        }
              
        runComponentsStateSaver();
        vm.fontsReady = true;
      }); 
      $document.scrollTopAnimated(500, 500);    
    } else if (Order.status == "edit") {
      vm.status = "edit";
      CanvasComponents.load(Order.components);
      
      runComponentsStateSaver();
      vm.fontsReady = true;
      $document.scrollTopAnimated(500, 500);      
    } else if (Order.status == "checkout") {
      vm.status = "checkout";
      vm.orderNum = Order.orderId;
      CanvasComponents.load(Order.components);
      runComponentsStateSaver();
      vm.fontsReady = true;
    }   

    initScopeWatchers();
    
  }


  function initScopeWatchers() {
    //обновить цену
    $scope.$watch(
      function() {
        return CanvasComponents.getTotalPrice();
      },
      function(newValue, oldValue) {
        vm.totalPrice = newValue;
      }
    );

    //обновить масимальную цену
    $scope.$watch(
      function() {
        return CanvasComponents.getMaxPrice();
      },
      function(newValue, oldValue) {
        vm.maxPrice = newValue;
      }
    );

    //обновить добавочную стоимость элемента, на который наведена мышь
    $scope.$watch(
      function() {
        return CanvasComponents.getMouseHoverAddPrice();
      },
      function(newValue, oldValue) {
        if (newValue==0) {
          vm.mouseHoverAddPrice = null;
        }
        else if (newValue>0) {
          vm.mouseHoverAddPrice = "+"+newValue;
        }
        else {
          vm.mouseHoverAddPrice = newValue;
        }
      }
    );

    //обновить описание
    $scope.$watch(
      function() {
        return CanvasComponents.getSpecification();
      },
      function(newValue, oldValue) {
        if (newValue != oldValue) {
          vm.specification = newValue;
        }
        if (vm.specification == null) {
          vm.specification = newValue;
        }
      }
    );

    //следить за изменением статуса
    $scope.$watch(
      function() {
        return Order.status;
      },
      function(newValue, oldValue) {
        if (newValue == "checkout") {
          if (WatchText.transformMode) {
            WatchText.transformModeOff();
          }
          if (WatchImage.transformMode) {
            WatchImage.transformModeOff();
          }         
          vm.status = "checkout";
          vm.orderNum = Order.orderId;
          window.scrollTo(0, 165);
        }
      }
    );
  }

  //для кнопки "новый макет"
  function createNewWatch() {
    Order.clearOrder();
    Order.createOrder().then(function() {
      vm.status = "edit";
      Order.status = "edit";
      CanvasComponents.setAllDefault();
      CanvasComponents.saveComponentsState(true);
    });
  }

  function runComponentsStateSaver() {
    //проверка на наличие изменений в сотсояние конструктора
    //если они есть, то отправить на сервер
    setInterval(function() {
      CanvasComponents.saveComponentsState();
    }, 1500);
  }

  function shareVk() {
    SocialShare.vkontakte(Order.orderId).then(function(){
      vm.shippingRepostStatus = "success";
    });
  }

  function shareFacebook() {
    SocialShare.facebook(Order.orderId).then(function(){
      vm.shippingRepostStatus = "success";
    });   
  }

  function shareTwitter() {
    SocialShare.twitter(Order.orderId).then(function(){
      vm.shippingRepostStatus = "success";
    });   
  }

  this.$onDestroy = function () {
    ConstructorTips.reset();
  };
}