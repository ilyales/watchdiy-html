angular.module('WatchApp.Config').constant("StrapsConf", {
  items:[		
		{
			name:'STRAP-black-leather',
			title:'Натуральная кожа, черный матовый',
			price:1920,
			configModes:['male','female'],
			validCases:['casual'],
			strapType:'leather',
			previewImg:'/watch-elements/straps/previews/leather-black2.png',
			img:'/watch-elements/straps/leather-black.png',
		},
		{
			name:'STRAP-blue-leather',
			title:'Натуральная кожа, синий глянцевый',
			price:1920,
			configModes:['male','female'],
			validCases:['casual'],
			strapType:'leather',
			previewImg:'/watch-elements/straps/previews/leather-blue2.png',
			img:'/watch-elements/straps/leather-blue.png',
		},	
		{
			name:'STRAP-white-leather',
			title:'Кожаный ремешок белый, сатинированная фактура',
			price:2050,
			configModes:['male','female'],
			validCases:['casual'],
			strapType:'leather',
			previewImg:'/watch-elements/straps/previews/leather-white2.png',
			img:'/watch-elements/straps/leather-white.png',
		},	
	]

});
